# Service Mesh

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Deployment configuration for Linkerd

## Table of Contents

- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Usage

Install the Linkerd CLI tool and follow the Getting Started instructions until you install it to the cluster

```sh
# Check that the CLI is installed
linkerd version

# Check that the cluster is ready
linkerd check --pre

# Install
linkerd install | kubectl apply -f -

# Install the on-cluster metrics
linkerd viz install | kubectl apply -f -
```

Check the Linkerd dashboard at http://localhost:8080 after running the following proxy command

```sh
k port-forward -n linkerd-viz svc/web 8080:8084
```

## Expose

Create a new secret

```sh
k create secret generic -n linkerd-viz dashboard-htpasswd \
  --from-literal=auth=$(htpasswd -Bnb $admin $password)
```

Deploy dashboard ingress using Fleet

```yaml
kind: GitRepo
apiVersion: fleet.cattle.io/v1alpha1
metadata:
  name: linkerd-viz-dashboard
  namespace: fleet-local
spec:
  repo: https://gitlab.com/noroff-accelerate/cloud/projects/aks-microservices/aks-service-mesh
  branch: master
  targetNamespace: linkerd-viz
  paths:
    - linkerd-viz
```

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2020 Noroff Accelerate AS
